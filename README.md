# book-server

Calibre is a pretty useful ebook management library that I use to
manage all my ebooks. It does have a server which lets you access your
ebooks remotely and I have always found this useful for downloading
books to my mobile devices when needed. Recently that server hasn't
really been working for me so I wrote my own extremely simple server
that will access your calibre library and serve your books through a
webpage.

## Deployment

To get the server running there are a few options but they all rely on
the presence of the following environment variables that need to be
set on the computer you are going to run the server on:

```
BS_CALIBRE_DIR=[/path/to/your/calibre/library/directory/]
BS_USERNAME=[USERNAME FOR THE SERVER]
BS_PASSWORD=[PASSWORD FOR THE SERVER]
```

Optionally the BS_PORT variable can be used to set the port for the
server (default 3000).

These variables must be set in the environment of the running book
server.

I use Docker to run the server, alternatively, the server can also be
run as a java jar file. There are simple instructions for both these
methods below.

### Docker

If you use docker you can simply use the latest container from Docker
Hub, providing your local calibre library as a volume using the '-v'
flag. This container exposes port 3000 so you can also map this to the
port you want using the '-p' flag:

```
$ docker run -e BS_USERNAME=[username] \
			 -e BS_PASSWORD=[password] \
		 -e BS_CALIBRE_DIR=[/path/to/calibre] \
		 -v /path/to/calibre:/path/to/calibre \
		 -p 3800:3000 \
		 -it s312569/book-server:1.06
$
```

### Run the Uberjar

A jar file is included in the repository and this can be run in the
usual way. Assuming all the necessary environment variables are set
(see above):

```
$ java -jar book-server.jar
```

There are many ways to automate the starting and stopping of the
server depending on you OS of choice.
