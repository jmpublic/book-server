FROM clojure:alpine
RUN mkdir -p /app /app/resources
RUN apk update
RUN apk add sqlite
WORKDIR /app
COPY target/*-standalone.jar ./book-server.jar
COPY resources/public resources/public
CMD java -jar book-server.jar
EXPOSE 3000
