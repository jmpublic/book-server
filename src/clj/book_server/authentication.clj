(ns book-server.authentication
  (:require [buddy.sign.jwt :as jwt]
            [buddy.sign.util :as su]
            [buddy.core.keys :as ks]
            [buddy.auth :as ba]
            [buddy.core.nonce :as no]
            [buddy.auth.backends.token :refer [jwe-backend]]
            [buddy.auth.middleware :refer [wrap-authentication wrap-authorization]]
            [book-server.config :refer [config]]
            [clojure.java.io :as io]
            [clj-time.core :as t]
            [ring.util.http-response :as response]))

(def secret (no/random-bytes 32))

(defn- check-user [h]
  (if (and (= (:user h) (:username config)) (= (:password h) (:password config)))
    [true {:user (:username config)
           :exp (-> (t/plus (t/now) (t/seconds 1800000)) (su/to-timestamp))}]
    [false {:message "Not authorised"}]))

(def auth-backend (jwe-backend {:secret secret :options {:alg :a256kw :enc :a128gcm}}))

(defn authenticated? [req]
  (ba/authenticated? req))

(defn wrap-auth [handler]
  (-> (wrap-authentication handler auth-backend)
      (wrap-authorization auth-backend)))

(defn create-auth-token [req]
  (let [credentials (:params req)
        [ok? res] (check-user credentials)]
    (if ok?
      (response/ok {:token (jwt/encrypt res secret {:alg :a256kw :enc :a128gcm})
                    :username (:user credentials)})
      (response/unauthorized res))))
