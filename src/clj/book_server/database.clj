(ns book-server.database
  (:require [clojure.java.jdbc :as jdbc]
            [jdbc.pool.c3p0 :as pool]
            [ring.util.codec :as co]
            [ring.util.http-response :as response]
            [clojure.java.io :as io]
            [clojure.string :as st]
            [book-server.config :refer [config]]))

(def db-spec (pool/make-datasource-spec
                  {:classname "org.sqlite.JDBC"
                   :subprotocol "sqlite"
                   :subname (:db-file config)}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utilitites
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- file-bytes [f]
  (let [ary (byte-array (.length f))
        is (java.io.FileInputStream. f)]
    (.read is ary)
    (.close is)
    ary))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; book list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- book-data [{:keys [id]}]
  (let [dq (str "SELECT format, name as file, book as id ",
                "FROM data ",
                "WHERE book = ?")]
    (jdbc/query db-spec [dq id])))

(defn- get-cover [{:keys [has_cover path] :as book}]
  (if (= has_cover 1)
    (let [f (io/file (:cal-directory config) path "cover.jpg")]
      (-> (file-bytes f) co/base64-encode))))

(defn book-list [{:keys [search-text latest] :or {search-text "" latest 0} :as req}]
  (let [bq (str "SELECT id, title, author_sort, isbn, path, timestamp, has_cover "
                "FROM books "
                "WHERE id > ? "
                "  AND (title LIKE ? OR author_sort like ? ) "
                "ORDER BY id "
                "LIMIT 10")
        cq (str "SELECT count(*) as count "
                "FROM books "
                "WHERE title LIKE ? "
                "  OR author_sort like ? ")
        s (str "%" search-text "%")
        books (jdbc/query db-spec [bq latest s s])
        fc (jdbc/query db-spec [cq s s] {:result-set-fn #(-> (first %) :count)})]
    (response/ok {:books (->> (map #(assoc % :formats (book-data %)) books)
                              (map #(assoc % :cover (get-cover %))))
                  :latest (if (> (count books) 0)
                            (apply max (map :id books))
                            0)
                  :count fc})))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; book download
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn get-book [{:keys [id format] :as req}]
  (let [q (str "SELECT b.path, d.name "
               "FROM books AS b "
               "LEFT JOIN data AS d ON b.id = d.book ",
               "WHERE b.id = ?")
        p (jdbc/query db-spec [q id] {:result-set-fn first})]
    (if (seq p)
      (let [r (io/file (:cal-directory config) (:path p)
                       (str (:name p) "." (st/lower-case format)))
            fname (-> (st/replace (:name p) #"[\s,']" "")
                      (str "." (st/lower-case format)))]
        (-> (response/ok (java.io.FileInputStream. r))
            (response/content-type "application/Epub")
            (response/header "Content-Disposition" fname)))
      (response/not-found))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search ahead
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn books-like [{:keys [text] :as req}]
  (let [q (str "SELECT title, author_sort AS author "
               "FROM books "
               "WHERE title LIKE ? "
               "  OR author_sort like ? "
               "LIMIT 10")
        s (str "%" text "%")
        p (jdbc/query db-spec [q s s] {:result-set-fn #(mapv :title %)})]
    (response/ok p)))
