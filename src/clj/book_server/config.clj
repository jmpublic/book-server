(ns book-server.config
  (:require [environ.core :refer [env]]
            [clojure.java.io :as io]
            [me.raynes.fs :as fs]
            [clojure.spec.alpha :as spec]))

(spec/def ::db-file fs/exists?)
(spec/def ::cal-directory fs/exists?)
(spec/def ::password (spec/and string? #(not (= % "PASSWORD"))))
(spec/def ::username (spec/and string? #(not (= % "USERNAME"))))
(spec/def ::port int?)
(spec/def ::config (spec/keys :req-un [::db-file ::password ::username ::port]))

(defn make-config
  []
  {:post [(spec/valid? ::config %)]}
  (let [r {:db-file (io/file (env :bs-calibre-dir) "metadata.db")
           :cal-directory (io/file (env :bs-calibre-dir))
           :password (env :bs-password)
           :username (env :bs-username)
           :port (Integer/parseInt (or (env :bs-port) "3000"))}]
    (spec/explain ::config r)
    r))

(def config (make-config))
