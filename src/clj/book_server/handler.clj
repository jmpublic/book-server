(ns book-server.handler
  (:require [compojure.core :refer [GET POST ANY defroutes routes wrap-routes]]
            [compojure.route :refer [resources]]
            [ring.util.response :refer [resource-response]]
            [ring.middleware.reload :refer [wrap-reload]]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.keyword-params :refer [wrap-keyword-params]]
            [ring.middleware.json :refer [wrap-json-response wrap-json-params]]
            [ring.middleware.file :only [wrap-file]]
            [ring.middleware.resource :only [wrap-resource]]
            [ring.middleware.content-type :only [wrap-content-type]]
            [ring.middleware.not-modified :only [wrap-not-modified]]
            [ring.middleware.cors :refer [wrap-cors]]
            [ring.adapter.jetty :refer :all]
            [book-server.database :as db]
            [book-server.authentication :as auth]
            [book-server.config :refer [config]]
            [hiccup.core :as hiccup]
            [ring.util.http-response :as response])
  (:gen-class))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; landing page
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn landing-page [_]
  (hiccup/html
   [:head
    [:meta {:charset "UTF-8"}]
    [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
    [:title "BookServer"]
    [:link
     {:rel "stylesheet"
      :href "https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.4/css/bulma.min.css"}]
    [:script {:defer true :src"https://use.fontawesome.com/releases/v5.3.1/js/all.js"}]
    [:link {:rel "apple-touch-icon" :sizes "180x180" :href "imgs/apple-touch-icon.png"}]
    [:link {:rel "icon" :type "image/png" :sizes "32x32" :href "imgs/favicon-32x32.png"}]
    [:link {:rel "icon" :type "image/png" :sizes "16x16" :href "imgs/favicon-16x16.png"}]
    [:link {:rel "manifest" :href "imgs/site.webmanifest"}]
    [:link {:rel "mask-icon" :href "imgs/safari-pinned-tab.svg" :color "#5bbad5"}]
    [:link {:rel "shortcut icon" :href "imgs/favicon.ico"}]
    [:meta {:name "msapplication-TileColor" :content "#2d89ef"}]
    [:meta {:name "msapplication-config" :content "imgs/browserconfig.xml"}]
    [:meta {:name "theme-color" :content "#ffffff"}]]
   [:body {:class "has-navbar-fixed-top"}
    [:div {:id "app"}]
    [:script {:src "js/compiled/app.js" :type "text/javascript"}]
    [:script "book_server.core.init();"]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; routing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn home [req]
  (resource-response "index.html" {:root "public"}))

(defn logo [req]
  (resource-response "imgs/bs.png"))

(defn book-list [req]
  (let [r (auth/authenticated? req)]
    (if r
      (db/book-list (:params req))
      (response/unauthorized {:message "Not authorised"}))))

(defn get-book [req]
  (let [r (auth/authenticated? req)]
    (if r
      (db/get-book (:params req))
      (response/unauthorized {:message "Not authorised"}))))

(defn search-ahead [req]
  (let [r (auth/authenticated? req)]
    (if r
      (db/books-like (:params req))
      (response/unauthorized {:message "Not authorised"}))))

(defroutes download-routes
  (POST "/book" [] get-book))

(defroutes most-routes
  (POST "/books" [] book-list)
  (POST "/login" [] auth/create-auth-token)
  (POST "/searchah" [] search-ahead)
  (ANY "*" req (landing-page req))
  (resources "/"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; handler
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn wrap-print-request [handler]
  (fn [req]
    (let [response (handler req)]
      (println response)
      response)))

(def handler
  (wrap-cors (routes
              (-> download-routes
                  (wrap-routes wrap-reload)
                  (wrap-routes auth/wrap-auth)
                  (wrap-defaults (assoc-in site-defaults [:security :anti-forgery] false))
                  (wrap-routes wrap-keyword-params)
                  (wrap-routes wrap-json-params))
              (-> most-routes
                  (wrap-routes wrap-reload)
                  (wrap-routes auth/wrap-auth)
                  (wrap-defaults (assoc-in site-defaults [:security :anti-forgery] false))
                  (wrap-routes wrap-keyword-params)
                  (wrap-routes wrap-json-params)
                  (wrap-routes wrap-json-response)))
             :access-control-allow-origin [#".*"]
             :access-control-allow-methods [:get :post]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; utilities
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn start-server []
  (let [port (:port config)]
    (run-jetty handler {:port port :join? false})))
