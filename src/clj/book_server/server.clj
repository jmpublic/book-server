(ns book-server.server
  (:require [book-server.handler :refer [handler]]
            [ring.adapter.jetty :refer [run-jetty]]
            [book-server.config :refer [config]])
  (:gen-class))

(defn -main [& args]
  (run-jetty handler {:port (:port config) :join? false}))
