(ns book-server.core
  (:require
   [reagent.core :as reagent]
   [re-frame.core :as re-frame]
   [book-server.events :as events]
   [book-server.views :as views]
   [book-server.config :as config]))

;; (defn on-js-reload []
;;   ;; optionally touch your app-state to force rerendering depending on
;;   ;; your application
;;   (swap! app-state update-in [:__figwheel_counter] (fnil inc 0)))

(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel] (.getElementById js/document "app")))

(defn ^:export init []
  (re-frame/dispatch-sync [::events/initialize-db])
  (dev-setup)
  (mount-root))
