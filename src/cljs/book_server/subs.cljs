(ns book-server.subs
  (:require
   [re-frame.core :as re-frame]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; db
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; routing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::active-panel
 (fn [db _] (:active-panel db)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; login
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::username
 (fn [db _] (:username db)))

(re-frame/reg-sub
 ::token
 (fn [db _] (:token db)))

(re-frame/reg-sub
 ::login-failed
 (fn [db _] (:login-failed db)))

(re-frame/reg-sub
 ::token
 (fn [db _] (:token db)))

(re-frame/reg-sub
 ::authenticating?
 (fn [db _] (:authenticating? db)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; books
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::books
 (fn [db _] (:books db)))

(re-frame/reg-sub
 ::book-count
 (fn [db _] (count (:books db))))

(re-frame/reg-sub
 ::library-count
 (fn [db _] (:library-count db)))

(re-frame/reg-sub
 ::prepping
 (fn [db _] (:prepping db)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::lookahead-res
 (fn [db _] (:lookahead-res db)))

(re-frame/reg-sub
 ::latest
 (fn [db _] (:latest db)))

(re-frame/reg-sub
 ::full-count
 (fn [db _] (:full-count db)))

(re-frame/reg-sub
 ::search-text
 (fn [db _] (:search-text db)))

(re-frame/reg-sub
 ::booking?
 (fn [db _] (:booking? db)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hamburger
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-sub
 ::hamburger
 (fn [db _] (:hamburger db)))
