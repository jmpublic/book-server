(ns book-server.events
  (:require
   [re-frame.core :as re-frame]
   [book-server.db :as db]
   [day8.re-frame.http-fx]
   [ajax.core :as ajax]
   [ajax.protocols :as prot]
   [goog.string :as string]
   [book-server.config :as co]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; init
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; intervals
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defonce interval-handler
  (let [live-intervals (atom {})]
    (fn handler [{:keys [action id frequency event]}]
      (condp = action
        :clean   (doall (map #(handler {:action :end :id %1})
                             (keys @live-intervals)))
        :start (if (get @live-intervals id)
                 (do (js/clearInterval (get @live-intervals id))
                     (swap! live-intervals assoc id
                            (js/setInterval #(re-frame/dispatch event) frequency)))
                 (swap! live-intervals assoc id
                        (js/setInterval #(re-frame/dispatch event) frequency)))
        :end (do (js/clearInterval (get @live-intervals id))
                 (swap! live-intervals dissoc id))))))

(interval-handler {:action :clean})

(re-frame.core/reg-fx
 :interval
 interval-handler)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; routing
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ panel-name]] (assoc db :active-panel panel-name)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; login
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-db
 ::clear-fail-login
 (fn [db [_]] (assoc db :login-failed false)))

(re-frame/reg-event-fx
 ::login
 (fn [{db :db} [_ username password]]
   {:http-xhrio {:method          :post
                 :uri             (do
                                    (println co/url)
                                    (str co/url "login"))
                 :params          {:user username :password password}
                 :timeout         5000
                 :format          (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success      [::good-login]
                 :on-failure      [::bad-login]}
    :db (assoc db :authenticating? true)}))

(re-frame/reg-event-fx
 ::good-login
 (fn [{db :db} [_ response]]
   {:db (-> db
            (assoc :authenticating? false)
            (assoc :token (:token response))
            (assoc :username (:username response))
            (assoc :active-panel :home-panel)
            (assoc :load-books true))
    :dispatch [::get-books "" 0]}))

(re-frame/reg-event-db
 ::bad-login
 (fn [db [_ response]]
   (assoc db :login-failed true :authenticating? false)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; activity monitor
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-fx
 ::set-active
 (fn [{db :db} _]
   {:interval {:action :start
               :id :activity
               :frequency 1800000
               :event [::timeout-logout]}}))

(re-frame/reg-event-fx
 ::timeout-logout
 (fn [{db :db} _]
   {:db (assoc db :username false :token nil :books [])
    :interval {:action :end
               :id :activity}}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; book list
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-fx
 ::get-books
 (fn [{db :db} [_ text latest replace]]
   {:db (if replace
          (-> (assoc db :booking? true :search-text text)
              (assoc :books []))
          (assoc db :booking? true :search-text text))
    :http-xhrio {:method :post
                 :uri (str co/url "books")
                 :params {:latest latest :search-text text}
                 :timeout 5000
                 :headers {:Authorization (str "Token " (:token db))}
                 :format (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success [::good-book-req]
                 :on-failure [::bad-book-req]}}))

(re-frame/reg-event-db
 ::good-book-req
 (fn [db [_ response]]
   (let [db (-> (assoc db :books (-> (concat (:books db) (:books response)) vec))
                (assoc :full-count (:count response))
                (assoc :latest (:latest response))
                (assoc :booking? false))]
     (if (not (:library-count db))
       (assoc db :library-count (:count response))
       db))))

(re-frame/reg-event-db
 ::bad-book-req
 (fn [db [_ response]]
   (assoc db :books-failed true)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; download book
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-fx
 ::download-book
 (fn [{db :db} [_ id format filename]]
   {:http-xhrio {:method :post
                 :uri (str co/url "book")
                 :params {:id id :format format}
                 :headers {:Authorization (str "Token " (:token db))}
                 :format (ajax/json-request-format)
                 :response-format {:content-type "application/epub+zip"
                                   :description "epub"
                                   :read prot/-body
                                   :type :arraybuffer}
                 :on-success [::good-dl-req filename]
                 :on-failure [::bad-dl-req filename]}
    :db (assoc db :prepping (str format "-" id))}))

(re-frame/reg-event-fx
 ::good-dl-req
 (fn [{db :db} [_ filename response]]
   {::launch-dl {:response response :filename filename}
    :db (assoc db :prepping false)}))

(re-frame/reg-fx
 ::launch-dl
 (fn [{response :response filename :filename}]
   (let [b (js/Blob. (clj->js [response]) (js-obj "type" "application/epub+zip"))
         o (.createObjectURL js/URL b)
         l (.createElement js/document "a")]
     (set! (.-href l) o)
     (set! (.-download l) filename)
     (.appendChild js/document.body l)
     (.click l)
     (.revokeObjectURL js/URL l))))

(re-frame/reg-event-fx
 ::bad-dl-req
 (fn [_ [_ filename response]]
   {::alert-bdl {:response response :filename filename}}))

(re-frame/reg-fx
 ::alert-bdl
 (fn [{response :response filename :filename}]
   (js/alert (str "Download of " filename " failed: " (:status-text response)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-fx
 ::search-change
 (fn [{db :db} [_ text]]
   {:http-xhrio {:method :post
                 :uri (str co/url "searchah")
                 :params {:text text}
                 :timeout 5000
                 :headers {:Authorization (str "Token " (:token db))}
                 :format (ajax/json-request-format)
                 :response-format (ajax/json-response-format {:keywords? true})
                 :on-success [::good-sa-req]
                 :on-failure [::bad-sa-req]}}))

(re-frame/reg-event-db
 ::good-sa-req
 (fn [db [_ response]]
   (assoc db :lookahead-res response)))

(re-frame/reg-event-db
 ::bad-sa-req
 (fn [db [_ response]]
   (assoc db :lookahead-failure true)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; logout
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-fx
 ::logout
 (fn [{db :db}]
   {:db (assoc db :username false
               :token nil
               :books [])}))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; hamburger
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(re-frame/reg-event-db
 ::hamburger
 (fn [db [_]]
   (assoc db :hamburger (if (db :hamburger) false true))))
