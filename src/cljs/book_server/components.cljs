(ns book-server.components
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [book-server.subs :as subs]
            [book-server.events :as events]
            [clojure.string :as st]
            [goog.string :refer [urlEncode]]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; login
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn username [comp-data on-change kp-fn & {:keys [help-message] :or {help-message ""}}]
  [:div {:class "field"}
   [:label {:class "label"} "Username"]
   [:div {:class "control has-icons-left"}
    [:input {:class (str "input is-light") :type "text"
             :placeholder "Username" :value (:username @comp-data)
             :on-change #(on-change % :username)
             :auto-complete "username"
             :on-key-press #(kp-fn %)
             :auto-focus true}]
    [:span {:class "icon is-small is-left"}
     [:i {:class "fas fa-user"}]]]
   [:p {:class "help is-danger"} help-message]])

(defn password [comp-data on-change kp-fn]
  [:div {:class "field"}
   [:label {:class "label"} "Password"]
   [:div {:class "control has-icons-left"}
    [:input {:class (str "input is-light") :type "password"
             :placeholder "Password" :value (:password @comp-data)
             :on-change #(on-change % :password)
             :auto-complete "password"
             :on-key-press #(kp-fn %)}]
    [:span {:class "icon is-small is-left"}
     [:i {:class "fas fa-lock"}]]]])

(defn login-submit [comp-data on-click]
  [:div {:class "control"}
   [:button {:class (:bclass @comp-data)
             :disabled (or (= (:password @comp-data) "")
                           (= (:username @comp-data) ""))
             :on-click on-click}
    "Submit"]])

(defn login-reset [comp-data on-click]
  [:div {:class "control"}
   [:button {:class "button is-text" :on-click on-click} "Reset"]])

(defn login-form []
  (let [comp-data (reagent/atom {:username ""
                                 :password ""
                                 :bclass "button is-link"
                                 :htext ""})
        login-fail (re-frame/subscribe [::subs/login-failed])
        authenticating? (re-frame/subscribe [::subs/authenticating?])
        submit-fn (fn [e]
                    (let [un (:username @comp-data)
                          pw (:password @comp-data)]
                      (.preventDefault e)
                      (swap! comp-data assoc :bclass
                             "button is-link is-loading")
                      (re-frame/dispatch [::events/login un pw])))
        reset-fn (fn [e]
                   (.preventDefault e)
                   (swap! comp-data #(-> (assoc % :username "")
                                         (assoc :password "")))
                   (re-frame/dispatch [::events/clear-fail-login]))
        on-change (fn [e k]
                    (swap! comp-data assoc k (-> e .-target .-value)))
        kp-fn (fn [e]
                (if (= 13 (.-charCode e))
                  (submit-fn e)))]
    (fn []
      (if @authenticating?
        (swap! comp-data #(-> (assoc % :bclass "button is-link is-loading")))
        (swap! comp-data #(-> (assoc % :bclass "button is-link"))))
      [:div {:class "box"}
       [:form
        [username comp-data on-change kp-fn
         :help-message (if @login-fail "Username or password wrong" "")]
        [password comp-data on-change kp-fn]
        [:div {:class "field is-grouped"}
         [login-submit comp-data submit-fn]
         [login-reset comp-data reset-fn]]]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; count div
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- book-words [n]
  (condp = n
    0 " no books "
    1 " one book "
    (str n " books ")))

(defn count-div []
  (let [sc (re-frame/subscribe [::subs/full-count])
        lc (re-frame/subscribe [::subs/library-count])
        sbw (book-words @sc)
        lbw (book-words @lc)]
    [:div {:style {:padding-top "20px" :text-align "center"}}
     (if (= @lc 0)
       "No books in library"
       (str "Search returned " sbw " from " lbw " in library"))]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; book display
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- downloads [title {:keys [id format]}]
  (let [filename (str (urlEncode title) "." (st/lower-case format))
        prepping (re-frame/subscribe [::subs/prepping])
        idd (str format "-" id)
        df (fn [e]
             (.preventDefault e)
             (re-frame/dispatch [::events/download-book id format filename]))]
    [:a {:class "level-item" :aria-label "download" :key (str id format)}
     [:span {:on-click df} format]
     (if (= @prepping idd)
       [:span {:style {:padding-left "10px"}}
        [:i {:class "fas fa-spinner fa-pulse"}]])]))

(defn- book-box [{:keys [id author_sort title formats cover] :as book}]
  [:div {:class "box" :key (str title author_sort)}
   [:article {:class "media"}
    [:div {:class "media-left"}
     [:figure {:class "image is-64x64"}
      [:img {:class "img" :src (str "data:image/jpeg;base64," cover)}]]]
    [:div {:class "media-content"}
     [:div {:class "content"}
      [:p
       [:strong title]
       [:br]
       (:author_sort book)]
      [:nav {:class "level is-mobile"}
       [:div {:class "level-left"}
        (doall (map #(downloads title %) formats))]]]]]])

(defn books-div []
  (let [books (re-frame/subscribe [::subs/books])]
    (fn []
      [:div {:style {:padding-top "20px"}} (doall (map book-box @books))])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; search
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn search-panel []
  (let [looka (re-frame/subscribe [::subs/lookahead-res])
        comp-data (reagent/atom {:text "" :hide true})
        on-change (fn [e]
                    (let [v (-> e .-target .-value)]
                      (.preventDefault e)
                      (swap! comp-data #(assoc % :text v :hide false))
                      (re-frame/dispatch [::events/search-change v])))
        key-press (fn [e]
                    (when (= 13 (.-charCode e))
                      (swap! comp-data #(assoc % :hide true))
                      (re-frame/dispatch [::events/get-books (@comp-data :text)
                                          0 true])))
        clear-fn (fn [_]
                   (reset! comp-data {:text "" :hide true})
                   (re-frame/dispatch [::events/get-books "" 0 true]))]
    (fn []
      [:div
       [:div {:class "field"}
        [:p {:class "control has-icons-left has-icons-right"}
         [:input {:class "input" :type "text" :placeholder "search"
                  :list "test"
                  :on-input #(on-change %)
                  :on-key-press #(key-press %)
                  :value (@comp-data :text)}]
         [:span {:class "icon is-left"}
          [:i {:class "fas fa-search" :aria-hidden "true"}]]
         (if (not (= (@comp-data :test) ""))
           [:span {:class "icon is-right" :on-click #(clear-fn %)
                   :style {:pointer-events "initial"}}
            [:i {:class "fas fa-times"}]])
         (if (and (not (@comp-data :hide)) (> (count @looka) 0))
           [:datalist {:id "test"}
            (map (fn [x] ^{:key x}[:option x]) @looka)])]]])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; more button
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn more-results []
  (let [latest (re-frame/subscribe [::subs/latest])
        fc (re-frame/subscribe [::subs/full-count])
        bc (re-frame/subscribe [::subs/book-count])
        st (re-frame/subscribe [::subs/search-text])
        b? (re-frame/subscribe [::subs/booking?])
        submit-fn (fn [e]
                    (.preventDefault e)
                    (re-frame/dispatch [::events/get-books @st @latest false]))]
    (fn []
      [:div {:style {:padding-top "20px" :text-align "center"}}
       (cond @b?
             [:div {:class "icon"} [:i {:class "fas fa-spinner fa-spin"}]]
             (= @fc 0)
             [:span ""]
             (< @bc @fc)
             [:button {:class "button is-light" :on-click #(submit-fn %)}
              "Show more results"]
             (= @fc 0)
             "No results"
             :else
             "No more results")])))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; navbar
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn navbar []
  (let [on-click-fn (fn [e]
                      (.preventDefault e)
                      (re-frame/dispatch [::events/logout]))
        ham-click (fn [e]
                   (.preventDefault e)
                   (re-frame/dispatch [::events/hamburger]))
        user (re-frame/subscribe [::subs/username])
        hamact (re-frame/subscribe [::subs/hamburger])]
    (fn []
      [:nav {:class "navbar is-fixed-top is-info" :role "navigation"
             :aria-label "main navigation"}
       [:div {:class "navbar-brand"}
        [:a {:class "navbar-item" :href "https://bulma.io"}
         [:img {:src "imgs/bs.png"}]]
        [:a {:role "button" :class "navbar-burger burger" :aria-label "menu"
             :aria-expanded "false" :data-target "navBar" :on-click ham-click}
         [:span {:aria-hidden "true"}]
         [:span {:aria-hidden "true"}]
         [:span {:aria-hidden "true"}]]]
       [:div {:id "navBar" :class (if @hamact "navbar-menu is-active" "navbar-menu")}
        [:div {:class "navbar-start"}]
        (if @user
          [:div {:class "navbar-end"}
           [:a {:class "navbar-item is-right"
                :on-click #(on-click-fn %)}
            "Logout"]])]])))
