(ns book-server.views
  (:require [re-frame.core :as re-frame]
            [reagent.core :as reagent]
            [book-server.subs :as subs]
            [book-server.routes :as routes]
            [book-server.events :as events]
            [book-server.components :as comps]))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; login
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn login-panel []
  [:div {:class "hero is-light is-fullheight-with-navbar"}
   [:div {:class "hero-body"}
    [:div {:class "container"}
     [:div {:class "columns"}
      [:div {:class "column"}]
      [:div {:class "column"}
       [comps/login-form]]
      [:div {:class "column"}]]]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; about
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn about-panel []
  [:div {:class "hero is-light is-fullheight"}
     [:div {:class "hero-body"}
      [:div {:class "container"}
       [:div "This is the About Page."
        [:div [:a {:href (routes/url-for :home)} "go to Home Page"]]]]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; home
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn home-panel []
  [:div {:class "columns"}
   [:div {:class "column is-centered is-half is-offset-one-quarter"}
    [comps/search-panel]
    [comps/count-div]
    [comps/books-div]
    [comps/more-results]]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; main
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defn- panels [panel-name]
  (case panel-name
    :home-panel [home-panel]
    :login-panel [login-panel]
    [:div]))

(defn show-panel [panel-name]
  [panels panel-name])

(defn event-listeners [handler]
  (js/window.addEventListener "keydown" handler)
  (js/window.addEventListener "mousemove" handler)
  (js/window.addEventListener "mousewheel" handler)
  (js/window.addEventListener "touchmove" handler)
  (js/window.addEventListener "DOMMousescroll" handler)
  (js/window.addEventListener "MSPointerMove" handler)
  (js/window.addEventListener "resize" handler))

(defn cancel-listeners [handler]
  (js/window.removeEventListener "keydown" handler)
  (js/window.removeEventListener "mousemove" handler)
  (js/window.removeEventListener "mousewheel" handler)
  (js/window.removeEventListener "touchmove" handler)
  (js/window.removeEventListener "DOMMousescroll" handler)
  (js/window.removeEventListener "MSPointerMove" handler)
  (js/window.removeEventListener "resize" handler))

(defn outer-ui [panel]
  (reagent/create-class
   (let [handler #(re-frame/dispatch [::events/set-active])]
     {:component-did-mount #(event-listeners handler)
      :component-will-unmount #(cancel-listeners handler)
      :display-name "outer ui"
      :reagent-render (fn [panel]
                        [:div
                         [comps/navbar]
                         [:div {:class "section"}
                          [:div {:class "container"} panel]]])})))

(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])
        token (re-frame/subscribe [::subs/token])]
    (if @token
      [outer-ui [show-panel @active-panel]]
      [:div
       [comps/navbar]
       [show-panel :login-panel]])))
